package com.android.biometricdemo

import android.content.Context
import android.os.Bundle
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat


class HomeActivity : AppCompatActivity() {

    var switchCompat:SwitchCompat?=null

    companion object {
        val MY_PREFS_NAME = "MyPrefsFile"
        val SWITCH_ENABLE="switch_enable"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        switchCompat=findViewById(R.id.switchBtn)

        var prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE)
        var isEnable = prefs.getBoolean(SWITCH_ENABLE, false)

        if (isEnable) {
            switchCompat?.isChecked=true
        }
        else {
            switchCompat?.isChecked=false
        }

        switchCompat?.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                var editor = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit()
                if(isChecked) {
                    editor.putBoolean(SWITCH_ENABLE,true)
                }
                else {
                    editor.putBoolean(SWITCH_ENABLE,false)
                }
                editor.apply()

            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}