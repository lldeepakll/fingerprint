package com.android.biometricdemo

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.os.Handler
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.android.biometricdemo.HomeActivity.Companion.MY_PREFS_NAME
import com.android.biometricdemo.HomeActivity.Companion.SWITCH_ENABLE


class SplashActivity : AppCompatActivity() {

    var cancellationSignal:CancellationSignal?=null

    val authenticationCallback : BiometricPrompt.AuthenticationCallback
        get() = @RequiresApi(Build.VERSION_CODES.P)

        object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                super.onAuthenticationError(errorCode, errString)
                notifyUser("error ${errString}")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                super.onAuthenticationSucceeded(result)
                notifyUser("Sucess Authentication")
                startActivity(Intent(this@SplashActivity,HomeActivity::class.java))
                finish()
            }
        }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        checkBioMetricSupport()

        Handler().postDelayed(Runnable {

            var prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE)
            var isEnable = prefs.getBoolean(SWITCH_ENABLE, false)

            if (isEnable) {
                triggerFingerPrint()
            }
            else {
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
            }


        },2500)

    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun triggerFingerPrint() {
        val biometricPrompt:BiometricPrompt=BiometricPrompt.Builder(this@SplashActivity)
            .setTitle("Unlock Application")
            .setDescription("Confirm your fingerprint")
            .setNegativeButton("cancel",this.mainExecutor,
                DialogInterface.OnClickListener({ dialog, which ->
                notifyUser("Auth Canceled")
            })).build()

        biometricPrompt.authenticate(getCancelationSignal(),mainExecutor,authenticationCallback)
    }

    private fun checkBioMetricSupport():Boolean {

        val keyGuardedManager: KeyguardManager =getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        if (!keyGuardedManager.isKeyguardSecure) {
            notifyUser("Fingerprint autherntication is not enable")
            return false
        }
        return true
    }

    fun notifyUser(message:String) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }

    fun getCancelationSignal(): CancellationSignal {
        cancellationSignal= CancellationSignal()
        cancellationSignal?.setOnCancelListener {
            notifyUser("Authentication was canceled by user")
        }
        return cancellationSignal as CancellationSignal
    }

}